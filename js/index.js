
/*
author: peace.shi
from: CEP-研发资源平台
*/
var vm = new Vue({
	el:"#app",
	data:function(){
		return{
			data:[],//所有初始数据
			dic:{

			},//展示的数据
			dics:{},//处理后的数据 工号字典
			tag:[],//工号数组
			loop:{
				time:100,//循环时间
				interval:null
			},
			state:'running',//控制动画的启动停止
			set:[],
			result:[],
			level:3,
			count:3,
			show:{
				index:false,
				draw:false,
				allList:false,//展示所有中奖名单
				currentLuckyList:false,//展示当前中奖名单
			},
			prize:{
			},
			totalPrize:0,
			currentLuckyList:[],
			currentLevel:1,//当前抽奖的等级
			currentNumber:0,
			activeName:'未中奖',
			fileList:[],
			personData:[],
			prizeData:[],
			delay:300,
			cycle:0,
			column:0
		}
	},
	created(){
		this.loadData()
	},
	methods:{
		loadData:function () {
			var _this = this
			if(localStorage.getItem('data')){
				this.dics = JSON.parse(localStorage.getItem('data'))
	            _this.dataFormat()
			}
			if(localStorage.getItem('prize')){
				this.totalPrize = JSON.parse(localStorage.getItem('totalPrize'))
				this.prize = JSON.parse(localStorage.getItem('prize'))
	            _this.show.index = true
			}
		},
		personChange:function(a,b,c){
			var _this = this 
			var files = a.target.files;
	        var fileReader = new FileReader();
	        fileReader.onload = function(ev) {
	            try {
	                var data = ev.target.result
	                var workbook = XLSX.read(data, {
	                    type: 'binary'
	                }) // 以二进制流方式读取得到整份excel表格对象
	                var persons = []; // 存储获取到的数据
	            } catch (e) {
	                console.log('文件类型不正确');
	                return;
	            }
	            // 表格的表格范围，可用于判断表头是否数量是否正确
	            var fromTo = '';
	            // 遍历每张表读取
	            for (var sheet in workbook.Sheets) {
	                if (workbook.Sheets.hasOwnProperty(sheet)) {
	                    fromTo = workbook.Sheets[sheet]['!ref'];
	                    console.log(fromTo);
	                    persons = persons.concat(XLSX.utils.sheet_to_json(workbook.Sheets[sheet]));
	                    break; // 如果只取第一张表，就取消注释这行
	                }
	            }
	            
	            _this.personData = []
	            _this.dics = {}
	            persons.forEach(function(e){
	            	let obj = {
	            		state:e['是否提供照片']=='是'?1:0,
	            		order:e['序号'],
	            		name:e['姓名'],
	            		number:e['工号'],
	            		bingoState:0,
	            		level:null
	            	}
	            	_this.personData.push(obj)
	            	_this.dics[obj.number] = obj
	            })
	            _this.$message.success('成功导入'+_this.personData.length+'条人员数据')
	            localStorage.setItem('data',JSON.stringify(_this.dics))
	            _this.dataFormat()
	        };
	        // 以二进制方式打开文件
	        fileReader.readAsBinaryString(files[0]);
		},
		prizeChange:function(a){
			var _this = this 
			var files = a.target.files;
	        var fileReader = new FileReader();
	        fileReader.onload = function(ev) {
	            try {
	                var data = ev.target.result
	                var workbook = XLSX.read(data, {
	                    type: 'binary'
	                }) // 以二进制流方式读取得到整份excel表格对象
	                var persons = []; // 存储获取到的数据
	            } catch (e) {
	                console.log('文件类型不正确');
	                return;
	            }
	            // 表格的表格范围，可用于判断表头是否数量是否正确
	            var fromTo = '';
	            // 遍历每张表读取
	            for (var sheet in workbook.Sheets) {
	                if (workbook.Sheets.hasOwnProperty(sheet)) {
	                    fromTo = workbook.Sheets[sheet]['!ref'];
	                    console.log(fromTo);
	                    persons = persons.concat(XLSX.utils.sheet_to_json(workbook.Sheets[sheet]));
	                    break; // 如果只取第一张表，就取消注释这行
	                }
	            }
	            _this.totalPrize = 0
	            _this.prize = {}
	            persons.forEach(function(e){
	            	let obj = {
	            		order:e['序号'],
	            		level:e['等级'],
	            		name:e['名称'],
	            		total:e['奖品个数'],
	            		img:e['奖品图片'],
	            		done:0,
	            		list:[],
	            	}
	            	_this.totalPrize +=1
	            	_this.prize[obj.order] = obj
	            })
	            if(_this.totalPrize>0){
	            	_this.$message.success('成功导入'+_this.totalPrize+'条奖品数据')
	            	localStorage.setItem('prize',JSON.stringify(_this.prize))
		            localStorage.setItem('totalPrize',JSON.stringify(_this.totalPrize))
		            _this.show.index = true
	            }
	            
	        };
	        // 以二进制方式打开文件
	        fileReader.readAsBinaryString(files[0]);
		},
		dataFormat:function(){
			this.imgFilter()
			this.bingoFilter()
		},
		importData:function(file, callback){
			var excel = document.getElementById('excel-file')
			excel.click()
		},
		importPrize:function(file, callback){
			var prize = document.getElementById('prize-file')
			prize.click()
		},
		// 过滤没有头像的
		imgFilter:function(){
			var obj = this.dics
			var res = {}
			for(x in obj){
				if(obj[x].state==1){
					res[x] = obj[x]
				}
			}
			this.dic =  res
		},
		//过滤中奖了的
		bingoFilter:function(){
			var obj = this.dic
			var res = {}
			var total = 0
			for(x in obj){
				if(obj[x].bingoState==0){
					res[x] = obj[x]
					total +=1
				}
			}
			this.totalDic = total
			this.column = Math.ceil(total/10)<16?16:Math.ceil(total/10);
			console.log('total:'+total)
			this.insertRule(this.column)
			this.dic =  res
			this.cycle = this.delay * this.column
			console.log(this.dic)
		},
		start:function(level){
			//从数据中抽取未获奖的工号数组
			this.tag = this.calTag(this.dic)
			if(this.tag.length < this.currentNumber){
				this.$alert('抽奖人数超出总人数')
				return
			}
			console.log(this.tag)
			this.show.draw = true
			this.show.index = false
			this.state = 'running'
			if(this.loop.interval){
				console.log('no')
			}else{
				this.interval()
			}
		},
		restart:function(){
			this.show.draw = false
			this.show.index = true
			this.show.currentLuckyList = false
			this.imgFilter()
			this.bingoFilter()
		},
		interval:function(){
			var _this = this
			this.loop.interval = setInterval(function(){
				_this.result = _this.fcombination(_this.tag,_this.currentNumber)	
			},this.loop.time)
		},
		randomTag:function(e){
			e = this.set.length
			return Math.floor(Math.random()*e)
		},
		stop:function(){			
			this.state = 'paused'
			this.$forceUpdate()
			if(this.loop.interval){
				clearInterval(this.loop.interval)
				this.loop.interval = null
				console.log(this.result)
				this.showResult(this.result)
			}else{
				console.log('no')
			}			
		},
		showResult:function(arr){
			var _this = this
			this.currentLuckyList = []
			console.log("奖品等级"+this.currentLevel)
			console.log("抽奖人数"+this.currentNumber)
			this.prize[this.currentLevel].done = this.prize[this.currentLevel].done+this.currentNumber
			//单次中奖人员
			for(x in arr){
				this.dics[arr[x]].level = this.currentLevel
				this.dics[arr[x]].bingoState = 1 //中奖
				console.log(this.dics[arr[x]])
				this.prize[this.currentLevel].list.push(this.dics[arr[x]])
				this.currentLuckyList.push(this.dics[arr[x]])
			}
			// this.currentNumber = this.prize[this.currentLevel].total-this.prize[this.currentLevel].done
			this.currentNumber = 0
			localStorage.setItem('data',JSON.stringify(this.dics))
			localStorage.setItem('prize',JSON.stringify(this.prize))
			setTimeout(function(){
				_this.show.draw = false
				_this.show.currentLuckyList = true
			},2000)
		},
		//组合
		fcombination:function(arr, num){
			let array = JSON.parse(JSON.stringify(arr));
			var result = []
			for(var i=0;i<num;i++){
				let a = Math.floor(Math.random()*array.length)
				result.push(array[a])
				let b = array[a];
				array.splice(a,1)
			}
			return result
		},
		calIndex:function (index,rowNumber) {
			rowNumber = 10
			var cloumn = Math.floor(index/rowNumber)
			var row = (index % rowNumber)
			// console.log('第'+cloumn+'列,第'+row+'行')
			return [cloumn,row]
			// body...
		},
		calRotateY:function(e,col){
			var e = (e+1)%16
			e = e==0?16:e
			if(e>col/2){
				return 5*(col/2+1-e)
			}
			if(e<=col/2){
				return 5*(col/2-e)
			}
		},
		calTranslateZ:function(e,col){
			var e = (e+1)%16
			e = e==0?16:e
			if(e>col/2){
				return 15*(e-col)
			}
			if(e<=col/2){
				return 15*(1-e)
			}
		},
		calAnimate:function(e,col){
			var e = (e+1)%16
			e = e==0?16:e
			return e
		},
		calBingo:function(e) {
			if(this.result.indexOf(e)>=0){
				return true
			}else{
				return false
			}
		},
		calTag:function(obj){
			var arr = []
			var _this = this
			console.log(obj)
			for(x in obj){
				if(obj[x].bingoState == 0){
					arr.push(obj[x].number)
				}	
			}
			return arr
		},
		beginDraw:function(){
			if(this.currentNumber == 0){
				this.$alert('抽奖人数不足')
			}else{
				this.start()
			}
		},
		reduceLevel:function(){
			this.currentLevel = this.currentLevel==1?this.totalPrize:this.currentLevel-1
			this.currentNumber = 0
			// var currentNumber = this.prize[this.currentLevel].total-this.prize[this.currentLevel].done
			// this.currentNumber = currentNumber<=0?0:currentNumber
		},
		addLevel:function(){
			this.currentLevel = this.currentLevel==this.totalPrize?1:this.currentLevel+1
			this.currentNumber = 0
			// var currentNumber = this.prize[this.currentLevel].total-this.prize[this.currentLevel].done
			// this.currentNumber = currentNumber<=0?0:currentNumber
		},
		reduceNumber:function(){
			if(this.currentNumber == 0){
				return
			}
			// var rest = this.prize[this.currentLevel].total-this.prize[this.currentLevel].done
			// this.currentNumber = this.currentNumber==1?rest:(this.currentNumber-1)
			this.currentNumber = this.currentNumber-1
		},
		addNumber:function(){
			// if(this.currentNumber == 0){
			// 	return
			// }
			// var rest = this.prize[this.currentLevel].total-this.prize[this.currentLevel].done
			// this.currentNumber = this.currentNumber==rest?1:this.currentNumber+1
			this.currentNumber = this.currentNumber+1
		},
		showLuckList:function(){
			this.show.allList = true
		},
		closeLuckyList:function(){
			this.show.allList = false
		},
		handleClick:function(tab,event){

		},
		clearData:function(e){
			if(e == 1){
				this.$confirm('此操作将清空中奖信息, 是否继续?', '提示', {
		          confirmButtonText: '确定',
		          cancelButtonText: '取消',
		          type: 'warning'
		        }).then(()=>{
		        	for(x in this.prize){
						this.prize[x].list = []
						this.prize[x].done = 0
					}
					localStorage.setItem('prize',JSON.stringify(this.prize))
					for(x in this.dics){
						this.dics[x].bingoState = 0
						this.dics[x].level = null
					}
					this.dataFormat()
					localStorage.setItem('data',JSON.stringify(this.dics))
					this.$forceUpdate()
			          this.$message({
			            type: 'success',
			            message: '删除成功!'
			          });
		        }).catch(function(){
		                   
		        });
				
			}
			if(e == 2){
				this.$confirm('此操作将清空所有信息（人员信息，奖品信息，中奖信息）, 是否继续?', '提示', {
		          confirmButtonText: '确定',
		          cancelButtonText: '取消',
		          type: 'warning'
		        }).then(()=>{
		        	localStorage.removeItem('data')
					localStorage.removeItem('prize')
					localStorage.removeItem('totalPrize')
					location.reload()
			          this.$message({
			            type: 'success',
			            message: '删除成功!'
			          });
		        }).catch(function(){
		                   
		        });
				
			}
		},
		insertRule:function(column){
			// 16
			var width = 1920
			if(column>16){
				width += (column -16)*120
			}
			let style = document.createElement('style');
	        style.setAttribute('type', 'text/css');
	        document.head.appendChild(style);
	        let sheet = style.sheet;
	        //根据每一个时钟的数据为页面添加不同的keyframes
	        sheet.insertRule(
	          `@keyframes calAnimate`+column+`
					{
					    0%   {
					        transform:  translateX(0px)  translateZ(5px);
					        -webkit-transform: translateX(0px)  translateZ(5px);
					        opacity: 1;
					    }
					    49%  {
					        transform: translateX(`+(width/2+120)+`px) rotateY(0) translateZ(-105px);
					        -webkit-transform: translateX(`+(width/2+120)+`px) rotateY(0) translateZ(-105px);
					        opacity: 1;
					    }
					    98% {
					        transform: translateX(`+(width+120)+`px)  translateZ(0px);
					        -webkit-transform: translateX(`+(width+120)+`px)  translateZ(0px);
					        opacity: 1;
					    }
					    99% {
					        transform: translateX(`+(width+120)+`px)  translateZ(0px);
					        -webkit-transform: translateX(`+(width+120)+`px)  translateZ(0px);
					        opacity: 0;
					    }
					    99.5% {
					        transform: translateX(0px)  translateZ(5px);
					        -webkit-transform: translateX(0px)  translateZ(5px);
					        opacity: 0;
					    }
					    100% {
					        transform: translateX(0px)  translateZ(0px);
					        -webkit-transform: translateX(0px)  translateZ(0px);
					        opacity: 1;
					    }
					}`,0
	        );
		}
	}
})